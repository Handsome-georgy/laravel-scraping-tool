jQuery(function ($) {
    $("#scraping-btn").click(function (e) {
        $("#spin-image").show();
    });
});

function getData(selector) {
    var kids = $(selector).parents("tr").children('td');
    var data = {};

    data = {
        title: $(kids[0]).children("a").text(),
        download_url: $(kids[0]).children("a").attr('href'),
        version: $(kids[1]).text(),
        price: $(kids[2]).text(),
        image_url: $(kids[3]).find("img").attr("src"),
        sale_page: $(kids[3]).children('a').attr('href'),
        description: $(kis[7]).text(),
        last_update: $(kids[4]).text(),
        rating_score: $(kids[5]).text(),
        rating_total: $(kids[6]).text()
    };

    return data;
}

function sendData(data, method) {
    $.ajax({
        url: baseUrl + "/saveData",
        type: "POST",
        data: {
            data: data,
            method: method
        },
        success: function (response) {
            console.log(response);
        },
        error: function () {
            console.log("Error");
        }
    });
}

function addPlugin(selector) {
    var data = getData(selector);
    sendData(data, 'add');
    $(selector).attr("disabled", "true");
    var kids = $(selector).parents("tr").children('td');
    $(kids[7]).find("#save-caption").text("Saved");
}

function updatePlugin(selector) {
    var data = getData(selector);
    sendData(data, 'update');
    $(selector).attr("disabled", "true");
}

function deletePlugin(id) {
    $.ajax({
        url: baseUrl + '/deletePlugin',
        type: "POST",
        data: {
            id: id
        },
        success: function (response) {
            if (response == "success") {
                var selector = "#p-" + id;
                $(selector).remove();
            }
        },
        error: function () {

        }
    });
}

function show_description(type, data,selector) {
    $.ajax({
        url: baseUrl + '/getDescription',
        type: 'get',
        dataType: 'json',
        data: {
            type: type,
            data: data
        },
        beforeSend:function(){
            $(selector).parents("td").find(".progress-image").show();
        },
        success: function (response) {
            $(".progress-image").hide();
            $("#description-modal .modal-title").text(response.title);
            $("#description-modal .modal-body .description").html(response.description);
            $("#description-modal .modal-body .feature-data").html(response.feature_data);
            $("#description-modal .plugin-image").attr("src",response.image_url);
            $("#description-modal").modal('show');
        },
        error: function (error) {
            console.log(error);
        }
    });
}

function download(url,id,selector){
    console.log(url);
    $.ajax({
        url:baseUrl+'/download_plugin',
        type:'get',
        dataType:'json',
        data:{
            url:url,
            id:id
        },
        beforeSend:function(){
            $(selector).parents("td").find(".down-progress-image").show();
            $(selector).hide();
        },
        success:function(response){
            $(selector).parents("td").find(".down-progress-image").hide();
            
            if(response.success){
                $(selector).remove();
                console.log(response.error);
            }
            else{
                $(selector).show();
                alert(response.error);
            }
        },
        error:function(){

        }
    });
}