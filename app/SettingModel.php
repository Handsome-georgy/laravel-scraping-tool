<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SettingModel extends Model
{
    //
    protected $table = 'setting';
    protected $primaryKey = 'id';
    protected $fillable = [
    	'name','value'
    ];
}
