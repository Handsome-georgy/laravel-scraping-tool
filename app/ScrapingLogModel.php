<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScrapingLogModel extends Model
{
    //
    protected $table = 'scraping_log';
    protected $primaryKey = 'id';
    protected $fillable = [
    	'user_id','type'
    ];
}
