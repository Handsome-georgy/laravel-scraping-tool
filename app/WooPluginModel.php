<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WooPluginModel extends Model
{
    //
    protected $table = 'woo_plugin';
    protected $primaryKey = 'id';
    protected $fillable = [
    	'user_id','title','download_url','version','image_url','description','price','sale_page','latest_update_date'
    ];
}
