<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Goutte;
use App\User;
use App\WooPluginModel;
use Illuminate\Support\Facades\Storage;
use File;
use App\SettingModel;
class AppController extends Controller
{
    //
    public function __construct(){

    }
    public function index(){       
        $totalCount = WooPluginModel::all()->count();
        $url = "https://gplzone.com/updates";
        $crawler = Goutte::request('GET', $url);
        $updatesCount = $crawler->filter('section.new-content li>a')->count();

        $url = "https://gplzone.com/catalog";
        $crawler = Goutte::request('GET', $url);
        $catalogCount = $crawler->filter('section.new-content li>a')->count();
        
        $offset = SettingModel::where('name','offset')->get()->first()->value;
        return view('dashboard',compact('totalCount','updatesCount','catalogCount','offset'));
    }

    public function saveData(Request $request){
        $pluginData = $request->data;
        $method = $request->method;

        if($method=="add"){
            $newPlugin = new WooPluginModel();
            $newPlugin->title = $pluginData['title'];
            $newPlugin->download_url = $pluginData['download_url'];
            $newPlugin->version = $pluginData['version'];
            $newPlugin->image_url = $this->saveImage($pluginData['image_url']);
            $newPlugin->price = $pluginData['price'];
            $newPlugin->description = $pluginData['description'];
            $newPlugin->feature_data = $pluginData['feature_data'];
            $newPlugin->sale_page = $pluginData['sale_page'];
            $newPlugin->last_update = $pluginData['last_update'];
            $newPlugin->rating_score = $pluginData['rating_score'];
            $newPlugin->rating_total = $pluginData['rating_total'];     
            $newPlugin->save();
        }
        else{
            WooPluginModel::where('title',$pluginData['title'])->update($pluginData);
        }
    }
    public function delete(Request $request){
        $id = $request->id;
        $plugin = WooPluginModel::findOrFail($id);
        $file = public_path('images/plugin_images/').$plugin->image_url;
        File::delete($file);
        $plugin->delete();
        echo "success";
    }

    public function allPlugins(){
        $data = WooPluginModel::all();
        return view('all_plugins',compact('data'));
    }

    public function getDescription(Request $request){
        $type = $request->type;
        $data = $request->data;
        if($type=="local"){
            $plugin = WooPluginModel::findOrFail($data);
            $plugin->image_url = url('/images/plugin_images').'/'.$plugin->image_url;
            echo json_encode($plugin);
        }
        else{
            $crawler = Goutte::request('GET', $data);
            $plugin = array();

            //Title
            $temp = $crawler->filter(".title-single h1")->first();
            $plugin['title']= $temp->count()>0?$temp->text():"";

            // Description          
            $temp = $crawler->filter('#tab-description span>p')->first();
            $plugin['description'] = $temp->count() > 0 ? $temp->html() : "";

            // Image 

            $temp = $crawler->filter('.extension-cart-form>a');
            if ($temp->count() > 0) {
                $link = $temp->extract(array('href'));
                $plugin['image_url'] = $link[0];
            }
            
            // Meta Data
            $temp = $crawler->filter("#tab-description ul")->last();
            $plugin['feature_data']=$temp->html();
            
            echo json_encode($plugin);
        }
    }

    public function download(Request $request){
        $url = $request->url;
        $id = $request->id;
        $file = file_get_contents($url);

        $plugin = WooPluginModel::findOrFail($id);
        $path = public_path('donwload/').$plugin->title." ".$plugin->version.".zip";
        $plugin->local_zip_url = $path;
        $plugin->update();

        $insert = file_put_contents($path,$file);
        if($insert){
            echo json_encode(array('success'=>'Successfully download'));
        }
        else{
            echo json_encode(array('error'=>'Download Failed'));
        }
    }
}
