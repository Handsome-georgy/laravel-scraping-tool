<?php

namespace App\Http\Controllers;

use App\ScrapingLogModel;
use App\WooPluginModel;
use Goutte;
use Illuminate\Http\Request;
use App\SettingModel;
use Symfony\Component\DomCrawler\Crawler;
class ScrapController extends Controller
{
    //
    public function __construct()
    {

    }

    public function index()
    {
        return view('scrap');
    }

    public function saveLog($type)
    {
        $newLog = new ScrapingLogModel();
        $newLog->type = $type;
        $newLog->save();
    }
    public function seperate($string, $type, $method)
    {
        $string = trim($string);
        $revString = strrev($string);
        if($method=="updates"){
            $pos = stripos($revString, "v");
            $version = substr($revString, 0, $pos+1);
            $title = substr($revString,$pos+2);          
        }            
        else{
            $pos = stripos($revString, " ");
            $version = substr($revString, 0, $pos+1).'v';
            $title = substr($revString,$pos+1);          
        }
        if($type=='title') return strrev($title);
        else return strrev($version);
    }
    
    //The plugin is already saved in local
    public function existLocal($title, $version)
    {
        if(wooPluginModel::where('title',$title)->count()>0){
            if(wooPluginModel::where('title',$title)->where('version',$version)->count()>0){
                return WooPluginModel::where('title',$title)->where('version',$version)->get()->first()->toArray();
            }
            return 'new_version';
        }
        return 'new_plugin';
    }

    public function scraping(Request $request)
    {
        $autoSave = $request->autosave;
        $type = isset($request->type) ? $request->type : "updates";
        $limit = $request->limit;
        $offset = SettingModel::where('name','offset')->select('value')->get()->first()->value;
        $this->saveLog($type);

        $data = array();
        $url = "https://gplzone.com/" . $type;
        $crawler = Goutte::request('GET', $url);
        $num = 0;

        $all = $crawler->filter('section.new-content li>a');
        
        foreach($all as $index=>$content){
        // $crawler->filter('section.new-content li>a')->each(function ($node, $index) use (&$data,&$num, &$autoSave, &$type, &$limit, &$offset) {
            if($type=="catalog"){
                if ( $index < $offset ) continue;
            }
            if($num==$limit) break;
            $num++;
            $node = new Crawler($content);
            $item = array();
            $href = $node->extract(array('href'));
            
            if($type == "updates"){
                $string = $node->text();                
            }
            else{                
                $string = $node->parents("li")->text();
            }

            $item['title'] = $this->seperate($string,'title',$type);
            $item['version'] = $this->seperate($string,'version',$type); 

            $item['download_url'] = trim($href[0]);
            $exist = $this->existLocal($item['title'],$item['version']);

            // if aleady exist in db, not necessary scraping
            if (is_array($exist)) {
                $item = $exist;
                $item['saved']=true;
                $item['type']="from_db";
            } else {
                $childCrawler = Goutte::request('GET', $href[0]);
                // Product Image URL
                $temp = $childCrawler->filter('.extension-cart-form>a');
                if ($temp->count() > 0) {
                    $link = $temp->extract(array('href'));
                    $item['image_url'] = $link[0];
                }

                // Description
                $temp = $childCrawler->filter('#tab-description span>p')->first();
                $item['description'] = $temp->count() > 0 ? $temp->html() : "";

                // Meta Data
                $temp = $childCrawler->filter("#tab-description ul")->last();
                $item['feature_data']=$temp->count()>0?$temp->html():"";
                // Price
                $temp = $childCrawler->filter(".subscription_form ul.subscription_benefits li>b")->first();
                $item['price'] = $temp->count() > 0 ? $temp->text() : "";

                // Sale Page
                $temp = $childCrawler->filter('div.notice>center')->last();
                $item['sale_page'] = $temp->count() > 0 ? $this->getSalePage(trim($temp->html())) : "";

                //Last Updated date
                $temp = $childCrawler->filter('ul.subscription_benefits li>b')->last();
                $item['last_update'] = $temp->count() > 0 ? $temp->text() : "";

                //zip download url

                // $item['zip_download_url']
                $temp = $childCrawler->filter('.rating_form_1')->first();
                $id = "";
                if ($temp->count() > 0) {
                    $id = $temp->extract(array('data-id'));
                    $id = substr($id[0],4);
                }   
                $item['zip_download_url'] = "https://gplzone.com/?download_file=".$id."&order=wc_order_5acdda4997be1&email=vuestube.fr@gmail.com";

                //rating score
                $temp = $childCrawler->filter('ul.rating_form li.rating_score')->first();
                $item['rating_score'] = $temp->count() > 0 ? $temp->text() : "";

                // //rating total
                $temp = $childCrawler->filter('ul.rating_form  li.rating_total span.votes')->first();
                $item['rating_total'] = $temp->count() > 0 ? $temp->text() : 0;

                $item['from_db']= false; 

                if ($autoSave) {
                    $item['image_url']=$this->savePlugin($item, $exist);
                    $item['saved']=true;
                    $item['type'] = $exist;
                }
                else{
                    $item['saved']=false;
                    $item['type']=$exist;
                }
            }
            array_push($data, $item);
        }
        if($type=="catalog"){
            $offset+=$num;
            SettingModel::where('name','offset')->update(['value'=>$offset]);
        }
        return view('scrap', compact('data', 'type'));
    }
    public function getSalePage($string)
    {
        $pos = strpos($string, "http");
        return substr($string, $pos);
    }
   
    public function savePlugin($pluginData)
    {
        $newPlugin = new WooPluginModel();
        $newPlugin->title = $pluginData['title'];
        $newPlugin->download_url = $pluginData['download_url'];
        $newPlugin->version = $pluginData['version'];
        $newPlugin->image_url = $this->saveImage($pluginData['image_url']);
        $newPlugin->description = $pluginData['description'];
        $newPlugin->feature_data = $pluginData['feature_data'];
        $newPlugin->price = $pluginData['price'];
        $newPlugin->zip_download_url = $pluginData['zip_download_url'];
        $newPlugin->sale_page = $pluginData['sale_page'];
        $newPlugin->last_update = $pluginData['last_update'];
        $newPlugin->rating_score = $pluginData['rating_score'];
        $newPlugin->rating_total = $pluginData['rating_total'];

        $newPlugin->save();

        return $newPlugin->image_url;
    }
}
