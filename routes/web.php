<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/dashboard',"AppController@index")->name('dashboard');
Route::get('scraping',"ScrapController@index")->name('scraping');
Route::post('scraping','ScrapController@scraping')->name('scraping');

Route::get('/',"AppController@index")->name('dashboard');
Route::get('/plugins',"AppController@allPlugins")->name('plugins');
Route::get('/getDescription',"AppController@getDescription");
Route::get('/download_plugin',"AppController@download");

Route::post('/saveData',"AppController@saveData");
Route::post('/deletePlugin','AppController@delete');

