<?php

use Illuminate\Database\Seeder;
use App\SettingModel;
class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $setting = new SettingModel();
        $setting->name = "offset";
        $setting->value = "0";
        $setting->save();
    }
}
