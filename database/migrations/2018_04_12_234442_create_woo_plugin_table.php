<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWooPluginTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('woo_plugin', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->string('title')->nullable();
            $table->string('download_url')->nullable();
            $table->string('zip_download_url')->nullable();
            $table->string('local_zip_url')->nullable();
            $table->string('version')->nullable();
            $table->string('image_url')->nullable();
            $table->text('description')->nullable();
            $table->text('feature_data')->nullable();
            $table->string('price')->nullable();
            $table->text('sale_page')->nullable();
            $table->string('last_update')->nullable();
            $table->string('rating_score')->nullable();
            $table->string('rating_total')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('woo_plugin');
    }
}
