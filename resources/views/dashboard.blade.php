@extends('layouts.dashboard_layout')
@section('content')
<div class="content">
    <!-- START Sub-Navbar with Header only-->
    <div class="sub-navbar sub-navbar__header">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-header m-t-0">
                        <h3 class="m-t-0">Dashboard</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Sub-Navbar with Header only-->

    <!-- START Sub-Navbar with Header and Breadcrumbs-->
    <div class="sub-navbar sub-navbar__header-breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 sub-navbar-column">
                    <div class="sub-navbar-header">
                        <h3>Dashboard</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Sub-Navbar with Header and Breadcrumbs-->


    <div class="container">

        <!-- START EDIT CONTENT -->
        <div class="row">
            <div class="col-lg-12 m-t-2">

                <!-- START 4 Boxes -->
                <div class="row">
                    <div class="col-lg-3 col-sm-3">
                        <div class="panel panel-default b-a-0 bg-gray-dark">
                            <div class="panel-heading bg-primary-i">
                                <div class="media">
                                    <div class="media-body">
                                        <span class="text-uppercsase">Store</span>
                                        <br>
                                        <h1 class="display-4 m-t-0 m-b-0">{{$totalCount}} Plugins                                            
                                        </h1>
                                    </div>
                                    <div class="media-right">
                                        <p class="data-attributes m-b-0">
                                            <span data-peity="{ &quot;fill&quot;: [&quot;#FFFFFF&quot;, &quot;#4ca8e1&quot;],  &quot;innerRadius&quot;: 20, &quot;radius&quot;: 28  }">2/7</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body p-t-3 p-b-0 text-center">
                                <div class="center-block">
                                    <span class="sparkline-bar">0:2,2:4,4:2,4:1,4:1,5:2,1:2,4:1,0:2,2:4,4:2,4:1,4:1,5:2,1:2,4:1</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-3">
                        <div class="panel panel-default b-a-0 bg-gray-dark">
                            <div class="panel-heading bg-success-i">
                                <div class="media">
                                    <div class="media-body">
                                        <span class="text-uppercsase">Today Updates</span>
                                        <br>
                                        <h1 class="display-4 m-t-0 m-b-0">
                                            {{$updatesCount}}
                                        </h1>
                                    </div>
                                    <div class="media-right">
                                        <p class="data-attributes m-b-0">
                                            <span data-peity="{ &quot;fill&quot;: [&quot;#FFFFFF&quot;, &quot;#98be68&quot;],  &quot;innerRadius&quot;: 20, &quot;radius&quot;: 28  }">8/10</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body p-t-3 p-b-0 text-center">
                                <div class="center-block">
                                    <span class="sparkline-bar">4:1,4:1,5:2,1:2,4:1,0:2,2:4,4:2,4:1,4:1,5:2,1:2,4:1,0:2,2:4,4:2</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-3">
                        <div class="panel panel-default b-a-0 bg-gray-dark">
                            <div class="panel-heading bg-warning-i">
                                <div class="media">
                                    <div class="media-body">
                                        <span class="text-uppercsase">Catalog</span>
                                        <br>
                                        <h1 class="display-4 m-t-0 m-b-0">
                                            {{$catalogCount}}
                                        </h1>
                                    </div>
                                    <div class="media-right">
                                        <p class="data-attributes m-b-0">
                                            <span data-peity="{ &quot;fill&quot;: [&quot;#FFFFFF&quot;, &quot;#ea825c&quot;], &quot;innerRadius&quot;: 20, &quot;radius&quot;: 28 }">3/10</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body p-t-3 p-b-0 text-center">
                                <div class="center-block">
                                    <span class="sparkline-bar">4:1,4:1,5:2,1:2,4:1,0:2,2:4,4:2,4:1,4:1,5:2,1:2,4:1,0:2,2:4,4:2</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-3">
                        <div class="panel panel-default b-a-0 bg-gray-dark">
                            <div class="panel-heading bg-danger-i">
                                <div class="media">
                                    <div class="media-body">
                                        <span class="text-uppercsase">Offset</span>
                                        <br>
                                        <h1 class="display-4 m-t-0 m-b-0">
                                            {{$offset}}
                                        </h1>
                                    </div>
                                    <div class="media-right">
                                        <p class="data-attributes m-b-0">
                                            <span data-peity="{ &quot;fill&quot;: [&quot;#FFFFFF&quot;, &quot;#ea825c&quot;], &quot;innerRadius&quot;: 20, &quot;radius&quot;: 28 }">3/10</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body p-t-3 p-b-0 text-center">
                                <div class="center-block">
                                    <span class="sparkline-bar">4:1,4:1,5:2,1:6,4:1,3:2,2:4,4:2,2:1,4:1,5:2,1:2,4:1,0:2,2:4,4:2</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END 4 Boxes -->

            </div>
        </div>
        <!-- END EDIT CONTENT -->

    </div>
    <script src="{{URL::to('/')}}/assets/vendor/js/jquery.sparkline.min.js"></script>
    <script src="{{URL::to('/')}}/assets/vendor/js/highstock.min.js"></script>
    <script src="{{URL::to('/')}}/assets/javascript/peity-settings.js"></script>
    <script src="{{URL::to('/')}}/assets/javascript/sparklines-settings.js"></script>
    <script src="{{URL::to('/')}}/assets/javascript/highchart-themes/highcharts&amp;highstock-theme.js"></script>
    <script src="{{URL::to('/')}}/assets/javascript/highchart-themes/highcharts-settings.js"></script>    
</div>

@endsection