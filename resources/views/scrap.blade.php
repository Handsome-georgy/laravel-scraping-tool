@extends('layouts.dashboard_layout') @section('content')
<style>
    .plugin-thumbnail{
        width:80px;
        height:auto;
        border:1px solid black;
        display:inline-block;
        margin:auto;
    }
    #spin-image{
        width:60px;
        display:none;
    }
</style>
<div class="content">
    <!-- START Sub-Navbar with Header only-->
    <div class="sub-navbar sub-navbar__header">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-header m-t-0">
                        <h3 class="m-t-0">Scraping</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Sub-Navbar with Header only-->

    <!-- START Sub-Navbar with Header and Breadcrumbs-->
    <div class="sub-navbar sub-navbar__header-breadcrumbs">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 sub-navbar-column">
                    <div class="sub-navbar-header">
                        <h3>Scraping</h3>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="col-lg-8">
                        <form method="post" role="form" action="{{URL::to('/scraping')}}">
                            <div class="col-sm-4">
                                {{ csrf_field() }}
                                @if(isset($type) && $type=="catalog")
                                    <label class="radio-inline">
                                        <input type="radio" name="type" value="catalog" checked> Catalog
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="type" value="updates"> Update
                                    </label>
                                @else
                                    <label class="radio-inline">
                                        <input type="radio" name="type" value="catalog"> Catalog
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="type" value="updates" checked> Update
                                    </label>
                                @endif
                                &nbsp;
                                <label class="checkbox-inline">
                                    <input type="checkbox" name="autosave" checked> Auto Save
                                </label>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group" style="display:inline;">
                                    <label for="inputEmail3" class="col-sm-4 control-label">Next Limit</label>
                                    <div class="col-sm-8" style="display:inline;">
                                        <select class="form-control" style="width:150px;" name="limit">
                                            <option value="5">5</option>
                                            <option  value="10">10</option>
                                            <option  value="30">30</option>
                                            <option  value="60">60</option>                                            
                                            <option  value="100">100</option>
                                        </select>
                                    </div>
                                </div>                                
                            </div>
                            <div class="col-sm-4">
                                <input id="scraping-btn" type="submit" class="btn btn-success" value="Scrap"/>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-4">
                        <img id="spin-image"  src="{{URL::to('/')}}/images/spin.gif"/>
                    </div>
                    
                    <br><br>
                    @if(isset($data))

                    <!-- START Zero Configuration -->
                    <table id="datatables-example" class="display table">
                        <thead>
                            <tr class="text-center">
                                <th width="30%">Title</th>
                                <th width="5%">Version</th>
                                <th width="5%">Price</th>
                                <th width="8%">Goto Shop</th>
                                <th width="10%">Updated Date</th>
                                <th width="7%">Rating Score</th>
                                <th width="7%">Rating Total</th>
                                <th width="4%">Detail</th>
                                <th width="10%">State</th>
                                <th width="auto">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $item)
                            <tr>
                                <td class="text-white">
                                    <a href="{{$item['download_url']}}" target="_blank">{{$item['title']}}</a><br>
                                    {{$item['download_url']}}
                                </td>
                                <td class="text-center"><br>{{$item['version']}}</td>
                                <td class="text-center"><br>{{$item['price']}}</td>
                                <td class="text-center">
                                    <a href="{{$item['sale_page']}}" target="_blank">
                                        @if($item['saved'])
                                            <img class="plugin-thumbnail" src="{{URL::to('/images/plugin_images')}}/{{$item['image_url']}}"/>
                                        @else
                                            <img class="plugin-thumbnail" src="{{$item['image_url']}}"/>
                                        @endif
                                    </a>
                                </td>
                                <td class="text-center"><br>{{$item['last_update']}}</td>
                                <td class="text-center"><br>{{$item['rating_score']}}</td>
                                <td class="text-center"><br>{{$item['rating_total']}}</td>
                                <td class="text-center">
                                    <div class="progress-image-div">
                                        <img class="progress-image" src="{{URL::to('/images/progress.svg')}}"/>
                                    </div>
                                    <i class="fa fa-eye btn" onclick="show_description('online','{{$item['download_url']}}',this)" style="margin-top:10px;"></i>
                                </td>
                                <td class="text-center">
                                    <br>
                                    @if($item['saved'])
                                        <span id="save-caption" class="label label-pill label-outline label-default" hidden>Saved</span>
                                    @else
                                        <span id="save-caption" class="label label-pill label-outline label-info">Unsaved</span>
                                    @endif
                                    &nbsp;
                                    @if($item['type']=="new_plugin")
                                        <span id="version-caption" class="label label-pill label-outline label-success">New</span>
                                    @elseif($item['type']=="new_version")
                                        <span id="version-caption" class="label label-pill label-outline label-primary">New Version</span>
                                    @else
                                        <span id="version-caption" class="label label-pill label-outline label-info">From DB</span>
                                    @endif
                                </td>
                                <td class="text-center" style="padding-top:20px;">
                                    <a href="{{$item['zip_download_url']}}">
                                        <button type="button" class="btn btn-outline btn-primary">
                                            <i class="fa fa-download fa-lg"></i>
                                        </button>
                                    </a>
                                    @if($item['saved'])
                                        <button type="button" class="btn btn-outline btn-primary" disabled>Add</button>
                                        <button type="button" class="btn btn-outline btn-success" disabled>Update</button>
                                    @else
                                        @if($item['type']=="new_version")
                                            <button type="button" class="btn btn-outline btn-primary" disabled>Add</button>
                                            <button type="button" class="btn btn-outline btn-success" onclick="updatePlugin(this)">Update</button>
                                        @else
                                            <button type="button" class="btn btn-outline btn-primary" onclick="addPlugin(this);">Add</button>
                                            <button type="button" class="btn btn-outline btn-success" disabled>Update</button>
                                        @endif
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <!-- END Zero Configuration -->

                    <script src="{{URL::to('/')}}/assets/vendor/js/jquery.dataTables.min.js"></script>
                    <script src="{{URL::to('/')}}/assets/vendor/js/dataTables.bootstrap.min.js"></script>
                    @endif
                </div>

            </div>
            
        </div>
        <div class="content" hidden><div class="container"></div> </div>
        <!-- END Sub-Navbar with Header and Breadcrumbs-->
        <script src="{{URL::to('/')}}/assets/vendor/js/jquery.sparkline.min.js"></script>
        <script src="{{URL::to('/')}}/assets/vendor/js/highstock.min.js"></script>
        <script src="{{URL::to('/')}}/assets/javascript/peity-settings.js"></script>
        <script src="{{URL::to('/')}}/assets/javascript/sparklines-settings.js"></script>
        <script src="{{URL::to('/')}}/assets/javascript/highchart-themes/highcharts&amp;highstock-theme.js"></script>
        <script src="{{URL::to('/')}}/assets/javascript/highchart-themes/highcharts-settings.js"></script>
        <script src="{{URL::to('/js/custom.js')}}"></script>
        
    </div>
    @endsection
