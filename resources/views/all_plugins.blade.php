@extends('layouts.dashboard_layout') @section('content')
<style>
    .plugin-thumbnail{
        width:80px;
        height:auto;
        border:1px solid black;
        display:inline-block;
        margin:auto;
    }    
</style>
<div class="content">
    <!-- START Sub-Navbar with Header only-->
    <div class="sub-navbar sub-navbar__header">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-header m-t-0">
                        <h3 class="m-t-0">All Plugins</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Sub-Navbar with Header only-->

    <!-- START Sub-Navbar with Header and Breadcrumbs-->
    <div class="sub-navbar sub-navbar__header-breadcrumbs">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 sub-navbar-column">
                    <div class="sub-navbar-header">
                        <h3>All Plugins</h3>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <!-- START Zero Configuration -->
                    <table id="datatables-example" class="display table">
                        <thead>
                            <tr class="text-center">
                                <th width="30%">Title</th>
                                <th width="5%">Version</th>
                                <th width="5%">Price</th>
                                <th width="10%">Goto Shop</th>
                                <th width="10%">Updated Date</th>
                                <th width="7%">Rating Score</th>
                                <th width="7%">Rating Total</th>
                                <th width="5%">Detail</th>
                                <th width="10%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $item)
                            <tr id="p-{{$item->id}}">
                                <td class="text-white">
                                    <a href="{{$item['download_url']}}" target="_blank">{{$item['title']}}</a><br>
                                    {{$item['download_url']}}
                                </td>
                                <td class="text-center"><br>{{$item['version']}}</td>
                                <td class="text-center"><br>{{$item['price']}}</td>
                                <td class="text-center">
                                    <a href="{{$item['sale_page']}}" target="_blank">
                                        <img class="plugin-thumbnail" src="{{URL::to('/images/plugin_images')}}/{{$item['image_url']}}"/>
                                    </a>
                                </td>
                                <td class="text-center"><br>{{$item['last_update']}}</td>
                                <td class="text-center"><br>{{$item['rating_score']}}</td>
                                <td class="text-center"><br>{{$item['rating_total']}}</td>
                                <td class="text-center">
                                    <div class="progress-image-div">
                                        <img class="progress-image" src="{{URL::to('/images/progress.svg')}}"/>
                                    </div>  
                                    <i class="fa fa-eye btn" onclick="show_description('local',{{$item['id']}},this)" style="margin-top:10px;"> 
                                    </i>
                                </td>
                                <td class="text-center" style="padding-top:20px;">
                                    @if($item['local_zip_url']==null)
                                        <img class="down-progress-image" src="{{URL::to('/images/progress.svg')}}"/>                                                                        
                                        <button type="button" class="btn btn-outline btn-primary" onclick="download('{{$item['zip_download_url']}}',{{$item->id}},this)">                                        
                                            <i class="fa fa-download fa-lg"></i>
                                        </button>
                                    @endif
                                    <button type="button" class="btn btn-outline btn-danger" onclick="deletePlugin({{$item->id}})">
                                        <i class="fa fa-trash fa-lg"></i>
                                    </button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <!-- END Zero Configuration -->

                    <script src="{{URL::to('/')}}/assets/vendor/js/jquery.dataTables.min.js"></script>
                    <script src="{{URL::to('/')}}/assets/vendor/js/dataTables.bootstrap.min.js"></script>
                </div>

            </div>
        </div>
    </div>
        <!-- END Sub-Navbar with Header and Breadcrumbs-->
    <div class="container"></div>
</div>
    @endsection
