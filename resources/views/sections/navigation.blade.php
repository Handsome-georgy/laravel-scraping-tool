<nav class="navigation">
    <!-- START Navbar -->
    <div class="navbar-inverse navbar navbar-fixed-top">
        <div class="container-fluid">

            <div class="navbar-header">
                <a class="current navbar-brand" href="{{URL::to('/')}}">
                    <img alt="Spin Logo Inverted" style="width:30px;" src="{{URL::to('/')}}/images/large_logo.png">
                </a>
                <button class="action-right-sidebar-toggle navbar-toggle collapsed" data-target="#navdbar" data-toggle="collapse" type="button">
                    <i class="fa fa-fw fa-align-right text-white"></i>
                </button>
                <button class="navbar-toggle collapsed" data-target="#navbar" data-toggle="collapse" type="button">
                    <i class="fa fa-fw fa-user text-white"></i>
                </button>
                <button class="action-sidebar-open navbar-toggle collapsed" type="button">
                    <i class="fa fa-fw fa-bars text-white"></i>
                </button>
            </div>

            <div class="collapse navbar-collapse" id="navbar">
                <!-- START Left Side Navbar -->
                <ul class="nav navbar-nav navbar-left clearfix yamm">
                    <!-- END Switch Sidebar ON/OFF -->

                    <!-- START Menu Only Visible on Navbar -->
                    <li id="top-menu-switch" class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Menu
                            <i class="fa fa-fw fa-caret-down"></i>
                        </a>
                    </li>
                    <!-- END Menu Only Visible on Navbar -->
                </ul>
                <!-- START Left Side Navbar -->

                <!-- START Right Side Navbar -->
                <ul class="nav navbar-nav navbar-right">

                    <li role="separator" class="divider hidden-lg hidden-md hidden-sm"></li>
                    <li class="dropdown-header hidden-lg hidden-md hidden-sm text-gray-lighter text-uppercase">
                        <strong>Your Profile</strong>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle user-dropdown" data-toggle="dropdown" href="#" role="button">
                            <span class="m-r-1">Kraig Schuster</span>
                            <div class="avatar avatar-image avatar-sm avatar-inline">
                                <img alt="User" src="https://s3.amazonaws.com/uifaces/faces/twitter/collegeman/128.jpg">
                            </div>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="dropdown-header text-gray-lighter">
                                <strong class="text-uppercase">Account</strong>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li>
                                <a href="../apps/profile-details.html">Your Profile</a>
                            </li>
                            <li>
                                <a href="../apps/profile-edit.html">Settings</a>
                            </li>
                            <li>
                                <a href="../apps/faq.html">Faq</a>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li>
                                <a href="../pages/login.html">Sign Out</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <!-- END Right Side Navbar -->
            </div>


        </div>
    </div>
    <!-- END Navbar -->

    <!-- START Sidebars -->
    <aside class="navbar-default sidebar">
        <div class="sidebar-overlay-head">
            <img src="../assets/images/spin-logo-inverted.png" alt="Logo" width="90">
            <a href="#" class="sidebar-switch action-sidebar-close">
                <i class="fa fa-times"></i>
            </a>
        </div>
        <div class="sidebar-logo">
            <img class="logo-default" src="../assets/images/spin-logo-big-inverse-@2X.png" alt="Logo" width="53">
            <img class="logo-slim" src="../assets/images/spin-logo-slim.png" alt="Logo" height="13">
        </div>

        <div class="sidebar-content">
            <div class="p-y-3 avatar-container">
                <img src="../assets/images/avatars/spin-avatar-woman.png" width="50" alt="Avatar" class="spin-avatar img-circle">
                <div class="text-center">
                    <h6 class="m-b-0">Michelle Baez</h6>
                    <small class="text-muted">Help Desk</small>
                    <p class="m-t-1 m-b-2">
                        <span id="sidebar-avatar-chart">5,3,2,-1,-3,-2,2,3,5,2</span>
                    </p>
                </div>
            </div>
            <div class="sidebar-default-visible text-muted small text-uppercase sidebar-section p-y-2">
                <strong>Navigation</strong>
            </div>

            <!-- START Tree Sidebar Common -->
            <ul class="side-menu">
                <li class="@if(\Illuminate\Support\Facades\Route::currentRouteName() == 'dashboard') active @endif">
                    <a href="{{URL::to('/dashboard')}}">
                        <i class="fa fa-home fa-lg fa-fw"></i>
                        <span class="nav-label">Dashboard</span>
                    </a>
                </li>
                <li class="@if(\Illuminate\Support\Facades\Route::currentRouteName() == 'scraping') active @endif">
                    <a href="{{URL::to('/scraping')}}">
                        <i class="fa fa-truck fa-lg fa-fw"></i>
                        <span class="nav-label">Scraping</span>
                    </a>
                </li>
                <li class="@if(\Illuminate\Support\Facades\Route::currentRouteName() == 'plugins') active @endif">
                    <a href="{{URL::to('/plugins')}}">
                        <i class="fa fa-cubes fa-lg fa-fw"></i>
                        <span class="nav-label">Plugin</span>
                    </a>
                </li>
            </ul>
            <!-- END Tree Sidebar Common  -->


            <div class="sidebar-default-visible">
                <hr>
            </div>

        </div>
    </aside>
    <!-- END Sidebars -->



</nav>