<!-- SCSS Output -->
<link rel="stylesheet" href="{{URL::to('/')}}/assets/stylesheets/app.min.e0bb64e7.css">
<!-- START Favicon -->

<link rel="icon" type="image/png" sizes="16x16" href="{{URL::to('/')}}/images/logo.png">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="{{URL::to('/')}}/assets/images/favicons/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
<!-- END Favicon -->

<!-- RSS -->
<link rel="alternate" type="application/rss+xml" title="RSS" href="{{URL::to('/')}}/atom.xml">
